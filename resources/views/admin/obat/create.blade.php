@extends('layouts.master')
@section('title')
    Obat
@endsection
@section('content')
    <div class="page-wrapper full-calender">
        <div class="page-body">
            <div class="row">


                <div class="row col-lg-12">
                    <h3><b>Add Obat</b></h3>
                </div>
                <div class="row col-lg-12">Welcome to Abuya Kangean Hospital<br><br></div>

                <div class="card row col-lg-12">
                    <div class="card-block">
                        <!-- Row start -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="tab-content card-block">
                                    <div class="tab-pane active" id="home3" role="tabpanel">
                                        <form method="post" action="{{ route('obat.store') }}">
                                            @csrf
                                            <p style="font-size:16px; color:red" align="center">
                                            <div class="form-group"> <label for="exampleInputEmail1">Nama Obat</label>
                                                <input type="text" class="form-control" id="obatName" name="obatName"
                                                    placeholder="Nama Obat" value="" required="true">
                                            </div>
                                            <div class="form-group"> <label for="exampleInputPassword1">Harga Obat</label>
                                                <input type="number" id="cost" name="obatPrice" class="form-control"
                                                    placeholder="Harga Obat" required="true">
                                            </div>

                                            <button type="submit" name="submit" class="btn btn-default">Add</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Row end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
