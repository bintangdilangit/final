@extends('layouts.master')
@section('title')
    Doctors
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Data Doctors</h2>

        </div>
        <div class="card-block">
            <div class="table-responsive dt-responsive">
                <table id="example1" class="table table-striped table-bordered nowrap">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Obat</th>
                            <th>Harga Obat</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($obats as $key => $obat)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td> {{ $obat->obatName }} </td>
                                <td> {{ $obat->obatPrice }} </td>
                                <td>
                                    <form action="{{ route('obat.destroy', ['obat' => $obat]) }}" style="display: inline;"
                                        method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger text-light">
                                            <i class="fa fa-trash" aria-hidden="true">
                                                Delete
                                            </i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });

    </script>
@endpush
