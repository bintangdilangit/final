@extends('layouts.master')
@section('title')
    Suster
@endsection
@section('content')
    <div class="page-wrapper full-calender">
        <div class="page-body">
            <div class="row">


                <div class="row col-lg-12">
                    <h3><b>Add Suster</b></h3>
                </div>
                <div class="row col-lg-12">Welcome to Abuya Kangean Hospital<br><br></div>

                <div class="card row col-lg-12">
                    <div class="card-block">
                        <!-- Row start -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="tab-content card-block">
                                    <div class="tab-pane active" id="home3" role="tabpanel">
                                        <form action="{{ route('suster.store') }}" method="POST" data-parsley-validate
                                            enctype="multipart/form-data">
                                            @csrf

                                            <div class="form-group"> <label for="exampleInputEmail1">Name</label> <input
                                                    type="text" class="form-control" id="name" name="name"
                                                    placeholder="Full Name" value="" required="true">
                                            </div>
                                            <div class="form-group"> <label for="exampleInputPassword1">Email</label>
                                                <input type="email" id="email" name="email" class="form-control"
                                                    placeholder="Email" value="" required="true">
                                            </div>
                                            <div class="form-group"> <label for="exampleInputEmail1">Mobile
                                                    Number</label> <input type="number" class="form-control" id="mobilenum"
                                                    name="tlp" placeholder="Mobile Number" value="" required="true"
                                                    maxlength="10" pattern="[0-9]+"> </div>
                                            <div class="radio">

                                                <p style="padding-top: 20px; font-size: 15px"> <strong>Gender:</strong>
                                                    <label>
                                                        <input type="radio" name="jeniskelamin" id="gender"
                                                            value="Perempuan">
                                                        Perempuan
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="jeniskelamin" id="gender"
                                                            value="Laki-laki">
                                                        Laki-laki
                                                    </label>
                                                </p>
                                            </div>
                                            <div class="form-group"> <label for="exampleInputEmail1">KTP</label> <input
                                                    type="file" name="ktp" id="ktp">
                                                </label>
                                            </div>
                                            <div class="form-group"> <label for="exampleInputEmail1">Ijazah</label>
                                                <input type="file" name="ijazah" id="ijazah">
                                                </label>
                                            </div>
                                            <div class="form-group"> <label for="exampleInputEmail1">Room</label>
                                                <select name="room_id" id="room_id" required="true" class="form-control">
                                                    <option value="">Pilih Room</option>
                                                    @foreach ($rooms as $rm)
                                                        <option value="{{ $rm->id }}">
                                                            {{ $rm->nameRoom }}
                                                    @endforeach
                                                    </option>

                                                </select>
                                                </label>
                                            </div>

                                            <br>
                                            <button type="submit" name="submit" class="btn btn-default">Add</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Row end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
