@extends('layouts.master')
@section('title')
    Doctor
@endsection
@section('content')
    <div class="page-wrapper full-calender">
        <div class="page-body">
            <div class="row">


                <div class="row col-lg-12">
                    <h3><b>Add Doctor</b></h3>
                </div>
                <div class="row col-lg-12">Welcome to Abuya Kangean Hospital<br><br></div>

                <div class="card row col-lg-12">
                    <div class="card-block">
                        <!-- Row start -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="tab-content card-block">
                                    <div class="tab-pane active" id="home3" role="tabpanel">
                                        <form method="post" action="{{ route('room.store') }}">
                                            @csrf
                                            <p style="font-size:16px; color:red" align="center">
                                            <div class="form-group"> <label for="exampleInputEmail1">Room Name</label>
                                                <input type="text" class="form-control" id="nameRoom" name="nameRoom"
                                                    placeholder="Room Name" value="" required="true">
                                            </div>
                                            <div class="form-group"> <label for="exampleInputPassword1">Room Price</label>
                                                <input type="number" id="cost" name="priceRoom" class="form-control"
                                                    placeholder="Room Price" value="" required="true">
                                            </div>
                                            <div class="form-group"> <label for="exampleInputPassword1">Level Room</label>
                                                <select name="level_id" id="level_id" required="true" class="form-control">
                                                    <option value="">Pilih Level Room</option>
                                                    @foreach ($levels as $lv)
                                                        <option value="{{ $lv->id }}">
                                                            {{ $lv->nameLevel }}
                                                    @endforeach
                                                    </option>

                                                </select>
                                            </div>

                                            <button type="submit" name="submit" class="btn btn-default">Add</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Row end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
