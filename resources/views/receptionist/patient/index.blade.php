@extends('layouts.master')
@section('title')
    Pasien
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Data Pasien</h2>

        </div>
        <div class="card-block">
            <div class="table-responsive dt-responsive">
                <table id="example1" class="table table-striped table-bordered nowrap">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>patient Name</th>
                            <th>Email</th>
                            <th>No. Telp</th>
                            <th>Jenis Kelamin</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($patient as $key => $patient)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td> {{ $patient->name }} </td>
                                <td> {{ $patient->email }} </td>
                                <td> {{ $patient->tlp }} </td>
                                <td> {{ $patient->jeniskelamin }} </td>
                                <td>
                                    <a href="{{ route('patient.edit', ['patient' => $patient]) }}" class="btn btn-info"><i
                                            class="fa fa-pencil"></i> Edit </a>

                                    <form action="{{ route('patient.destroy', ['patient' => $patient]) }}"
                                        style="display: inline;" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger text-light">
                                            <i class="fa fa-trash" aria-hidden="true">
                                                Delete
                                            </i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });

    </script>
@endpush
