@extends('layouts.master')
@section('title')
    Administrasi
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Data Administrasi</h2>
        </div>
        <div class="card-block">
            <div class="table-responsive dt-responsive">
                <table id="example1" class="table table-striped table-bordered nowrap">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Pasien</th>
                            <th>Nama Kamar</th>
                            <th>Nama Perawat</th>
                            <th>Harga Kamar</th>
                            <th>Harga Obat</th>
                            <th>Total Payment</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($bill as $key => $bill)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td> {{ $bill->patients->name }} </td>
                                <td> {{ $bill->susters->rooms->nameRoom }} </td>
                                <td> {{ $bill->susters->name }} </td>
                                <td> Rp.{{ $bill->susters->rooms->priceRoom }} </td>
                                <td> Rp.{{ $bill->obats->obatPrice }} </td>
                                <td> Rp.{{ $bill->susters->rooms->priceRoom + $bill->obats->obatPrice }} </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });

    </script>
@endpush
