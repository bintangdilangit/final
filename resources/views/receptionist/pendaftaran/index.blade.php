@extends('layouts.master')
@section('title')
    Pendaftaran
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <h2>List Pendaftaran</h2>

        </div>
        <div class="card-block">
            <div class="table-responsive dt-responsive">
                <table id="example1" class="table table-striped table-bordered nowrap">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Ruangan</th>
                            <th>Nama Pasien</th>
                            <th>Nama Perawat</th>
                            <th>Nama Doktor</th>
                            <th>Waktu Masuk</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pendaftaran as $key => $pendaftaran)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>{{ $pendaftaran->susters->rooms->nameRoom }} -
                                    {{ $pendaftaran->susters->rooms->levels->nameLevel }} </td>
                                <td> {{ $pendaftaran->patients->name }} </td>
                                <td> {{ $pendaftaran->doctors->name }} </td>
                                <td> {{ $pendaftaran->susters->name }} </td>
                                <td> {{ $pendaftaran->created_at }} </td>
                                <td>
                                    <a href="{{ route('pendaftaran.edit', ['pendaftaran' => $pendaftaran]) }}"
                                        class="btn btn-info"><i class="fa fa-pencil"></i> Edit </a>

                                    <form action="{{ route('pendaftaran.destroy', ['pendaftaran' => $pendaftaran]) }}"
                                        style="display: inline;" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger text-light">
                                            <i class="fa fa-trash" aria-hidden="true">
                                                Delete
                                            </i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });

    </script>
@endpush
