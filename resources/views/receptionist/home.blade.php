@extends('layouts.master')
@section('content')
    <div class="page-wrapper full-calender">
        <div class="page-body">
            <div class="row">
                <div class="col-xl-3 col-md-6">
                    <div class="card bg-c-green update-card">
                        <div class="card-block">
                            <div class="row align-items-end">
                                <div class="col-8">
                                    <h4 class="text-white">
                                        {{ $patientCount }}
                                    </h4>
                                    <h6 class="text-white m-b-0">Total Pasien</h6>
                                </div>
                                <div class="col-4 text-right">
                                    <canvas id="update-chart-2" height="50"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6">
                    <div class="card bg-c-pink update-card">
                        <div class="card-block">
                            <div class="row align-items-end">
                                <div class="col-8">
                                    <h4 class="text-white">
                                        {{ $pendaftaranCount }}
                                    </h4>
                                    <h6 class="text-white m-b-0">Total Pendaftaran</h6>
                                </div>
                                <div class="col-4 text-right">
                                    <canvas id="update-chart-3" height="50"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6">
                    <div class="card bg-c-lite-green update-card">
                        <div class="card-block">
                            <div class="row align-items-end">
                                <div class="col-8">
                                    <h4 class="text-white">
                                        {{ $susterCount }}
                                    </h4>
                                    <h6 class="text-white m-b-0">Total Suster
                                    </h6>
                                </div>
                                <div class="col-4 text-right">
                                    <canvas id="update-chart-4" height="50"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="card row col-lg-12">
        <div class="card-block">
            <!-- Row start -->
            <div class="row">
                <div class="col-lg-12">

                    <h3 class="title1">Edit Services</h3>

                    <div class="form-title">
                        <h4>Parlour Services:</h4>
                    </div>
                    @php
                        $id = Auth::user()->id;
                    @endphp
                    <div class="form-body">
                        <form method="post" action="{{ route('profile.update.recep', $id) }}"
                            enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <p style="font-size:16px; color:red" align="center">
                            <div class="form-group"> <label for="exampleInputEmail1">Username</label> <input type="text"
                                    class="form-control" id="username" name="username" placeholder="Username"
                                    value="{{ Auth::user()->username }}" required="true">
                            </div>
                            <div class="form-group"> <label for="exampleInputPassword1">First Name</label> <input
                                    type="text" id="firstname" name="firstname" class="form-control"
                                    placeholder="First Name" value="{{ Auth::user()->firstname }}" required="true">
                            </div>
                            <div class="form-group"> <label for="exampleInputPassword1">Last Name</label> <input type="text"
                                    id="lastname" name="lastname" class="form-control" placeholder="Last Name"
                                    value="{{ Auth::user()->lastname }}" required="true">
                            </div>
                            <div class="form-group"> <label for="exampleInputPassword1">Email</label> <input type="text"
                                    id="email" name="email" class="form-control" placeholder="Email"
                                    value="{{ Auth::user()->email }}" required="true"> </div>
                            <div class="form-group"> <label for="exampleInputPassword1">No. Telp.</label> <input type="text"
                                    id="tlp" name="tlp" class="form-control" placeholder="No. Telp."
                                    value="{{ Auth::user()->tlp }}" required="true"> </div>
                            <div class="form-group"> <label for="exampleInputPassword1">Address</label> <input type="text"
                                    id="address" name="address" class="form-control" placeholder="Address"
                                    value="{{ Auth::user()->address }}" required="true">
                            </div>

                            <div class="form-group">
                                <select id="gender" name="jeniskelamin" class="form-control">
                                    <option {{ Auth::user()->jeniskelamin == 'Laki-laki' ? 'selected' : '' }}
                                        value="Laki-laki">
                                        Laki-laki</option>
                                    <option {{ Auth::user()->jeniskelamin == 'Perempuan' ? 'selected' : '' }}
                                        value="Perempuan">
                                        Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                @if (Auth::user()->avatar != null)
                                    <label class="bmd-label-floating">Profile Picture | Selected
                                        ({{ Auth::user()->avatar }})</label>
                                @else
                                    <label class="bmd-label-floating">Profile Picture | Not Yet Selected</label>
                                @endif
                                <input type="file" id="avatar" name="avatar" class="form-control" placeholder=""
                                    value="{{ Auth::user()->avatar }}" required="true">
                            </div>

                            <button type="submit" name="submit" class="btn btn-info" style="float: right">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
