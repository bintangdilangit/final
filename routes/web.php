<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\BillController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ObatController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\PendaftaranController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\SusterController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('admin/home', [HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::prefix('/admin')->group(function (){
    Route::resource('/doctor', DoctorController::class);
    Route::resource('/suster', SusterController::class);
    Route::resource('/room', RoomController::class);
});
Route::prefix('/receptionist')->group(function (){
    Route::resource('/patient', PatientController::class);
    Route::resource('/pendaftaran', PendaftaranController::class);
    Route::resource('/obat', ObatController::class);
});


Route::get('/admin', [AdminController::class, 'admin'])->name('admin');
Route::put('/{admin}', [AdminController::class, 'update'])->name('admin.update');

Route::get('/profile', [ClientController::class, 'profile'])->name('profile');
Route::put('/{profile}', [ClientController::class, 'updateRecep'])->name('profile.update.recep');


Route::resource('/bill', BillController::class);
