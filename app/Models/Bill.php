<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    use HasFactory;
    public function bookings(){
        return $this->belongsTo(Booking::class, 'booking_id');
    }
    public function obats(){
        return $this->belongsTo(Obat::class, 'obat_id');
    }
}