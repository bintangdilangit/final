<?php

namespace App\Http\Controllers;

use App\Models\Obat;
use Illuminate\Http\Request;

class ObatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $obats = Obat::orderBy('created_at', 'desc')->get();
        return view('admin.obat.index',compact('obats'));
    }

    public function create()
    {
        return view('admin.obat.create');
    }

    public function store(Request $request)
    {
        $obat = new obat();
        $obat->obatName = $request->obatName;
        $obat->obatPrice = $request->obatPrice;
        $obat->save();
        return redirect(route('obat.index'));

    }
    public function destroy($id)
    {
        $obat = Obat::find($id);
        $obat->delete();
        return redirect(route('obat.index'));
    }
}