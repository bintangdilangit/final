<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Obat;
use App\Models\Patient;
use App\Models\Suster;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $patientCount = Patient::all()->count();
        $pendaftaranCount = Booking::all()->count();
        $susterCount = Suster::all()->count();
        return view('receptionist.home', compact('patientCount','pendaftaranCount','susterCount'));
    }

    public function adminHome()
    {
        $patientCount = Patient::all()->count();
        $pendaftaranCount = Booking::all()->count();
        $susterCount = Suster::all()->count();
        $obatCount = Obat::all()->count();
        $doctorCount = Obat::all()->count();
        return view('admin.adminHome', compact('patientCount','pendaftaranCount','susterCount','obatCount','doctorCount'));
    }
}