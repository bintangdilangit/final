<?php

namespace App\Http\Controllers;

use App\Models\Level;
use App\Models\Room;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $levels = Level::orderBy('created_at', 'desc')->get();
        $rooms = Room::orderBy('created_at', 'desc')->get();
        return view('admin.rooms.index',compact('rooms','levels'));
    }

    public function create()
    {
        $levels = Level::orderBy('created_at', 'desc')->get();
        return view('admin.rooms.create', compact('levels'));
    }

    public function store(Request $request)
    {
        $room = new Room();
        $room->nameRoom = $request->nameRoom;
        $room->priceRoom = $request->priceRoom;
        $room->level_id = $request->level_id;
        $room->save();
        return redirect(route('room.index'));

    }
    public function edit($id)
    {
        $rooms = Room::where('id', $id)->first();
        $levels = Level::orderBy('created_at', 'desc')->get();
        return view('admin.rooms.edit', compact('rooms','levels'));
    }

    public function update(Request $request, $id)
    {
        $room = Room::find($id);
        $room->nameRoom = $request->nameRoom;
        $room->priceRoom = $request->priceRoom;
        $room->level_id = $request->level_id;
        $room->update();
        return redirect(route('room.index'));

    }
    public function destroy($id)
    {
        $room = Room::find($id);
        $room->delete();
        return redirect(route('room.index'));
    }
}