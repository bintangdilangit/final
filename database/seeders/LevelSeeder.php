<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('levels')->insert(
            ['nameLevel' => 'I']
        );
        DB::table('levels')->insert(
            ['nameLevel' => 'IIA']
        );
        DB::table('levels')->insert(
            ['nameLevel' => 'IIB']
        );
        DB::table('levels')->insert(
            ['nameLevel' => 'III']
        );
    }
}